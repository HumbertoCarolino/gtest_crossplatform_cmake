#include "NameList.h"

#include <iostream>

#if _WIN32
#include <Windows.h>
#endif

#if _WIN32
int main(int argc, const char** argv)
{
    std::cout << "Fake main, for runtime" << std::endl;
}
#else

int main(int argc, const char** argv)
{
	std::cout << "Fake main, for runtime" << std::endl;
}
#endif